import {Component} from 'angular2/core';
@Component({
  selector: 'app-header',
  template: `
    <h1>{{pageTitle}}</h1>
    <h2>{{pageSubtitle}}</h2>
  `
})
export class AppHeader {
  private pageTitle: string = "Demo Application";
  private pageSubtitle: string = "Using Electron and Angular2";
}
